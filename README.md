
# Project product-infinite

## About
Welcome to the product list management project! This project is built with Vue.js and Vite, utilizing plugins such as vite-layout, vite-page, and vite-auto-import to optimize the development process. Product data is managed using Pinia and stored in local storage to ensure a seamless user experience.

![Logo](https://res.cloudinary.com/dvdejvb2x/image/upload/v1706324413/w5r0m4gvrrt0mquik6q7.jpg)

## Function

#### List of Products:
- The homepage will display a list of products.
- As you scroll down, an additional 20 products will be loaded for viewing.

#### Search Functionality:
- Provide the ability to search by product name.

## Setup and Run

#### Project setup
- Use command to install yarn:
```bash
  yarn
```

#### Run project
- Use command to start the program:
```bash
  yarn run dev
```
- Open a browser and visit http://localhost:5173/ to view the application.

## Demo
Click to [link demo deploy](https://master--bucolic-gumption-4d3459.netlify.app/)


## Plugins and Libraries Used:
#### Vite Plugins:
- vite-plugin-vue-layouts
- vite-plugin-pages
- unplugin-vue-components/vite
- unplugin-auto-import/vite
- vite-plugin-pwa
#### Dependencies
- axios
- pinia
- vee-validate
- vuetify3
- tailwindcss
- lodash

## 🚀 About Me
I am a front-end developer with 1 year of experience in programming languages such as HTML/CSS/JS/Typescript and frameworks like ReactJS/NextJS/VueJS/NuxtJS. I have researched, worked with, and applied various technologies and libraries in practical scenarios, including Socket, Sentry, AWS, Firebase, Docker, Jenkins, Redux Toolkit, Figma, etc. I am familiar with UI frameworks like MUI, Antd, Vuetify, Tailwind, and have a solid understanding of front-end development concepts and tools.

## 🔗 Links
[![portfolio](https://img.shields.io/badge/my_portfolio-10B981?style=for-the-badge&logo=ko-fi&logoColor=white)](https://minhchanh013.github.io/my_portfolio/)
[![Gitlab](https://img.shields.io/badge/gitlab-FC6D26?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/Chanh013)
[![twitter](https://img.shields.io/badge/github-000?style=for-the-badge&logo=github&logoColor=white)](https://github.com/MinhChanh013)
