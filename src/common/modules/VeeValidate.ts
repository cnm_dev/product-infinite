import type { UserModule } from "../types/models.js";
import { configure, defineRule } from "vee-validate";
import en from "@vee-validate/i18n/dist/locale/en.json";
import {
  email,
  max,
  min,
  numeric,
  required,
  alpha_num,
  regex,
  confirmed,
  length,
} from "@vee-validate/rules";
import dayjs from "dayjs";
import { localize } from "@vee-validate/i18n";

// Setup Pinia
// https://pinia.esm.dev/
export const install: UserModule = ({}) => {
  interface Param {
    [index: string]: any;
  }

  defineRule("length", (v: any, params: any) => {
    if (!v) return true;
    return length(v, params);
  });
  defineRule("max", max);
  defineRule("min", min);
  defineRule("numeric", numeric);
  defineRule("confirmed", confirmed);
  defineRule("alpha_num", alpha_num); // eslint-disable-line
  defineRule("required", (v: any) => {
    if ([false].includes(v)) return true;
    return required(v);
  });
  defineRule("regex", regex);
  defineRule("email", email);
  // 時刻フォーマット
  defineRule("time_format", (value: string) => {
    if (value == null) return true;
    return /^([01][0-9]|2[0-3]):([0-5][0-9])$/.test(value);
  });
  // 現在時刻の10分後以降の場合true
  defineRule("available_time", (value: string, { day }: Param) => {
    if (!day) {
      return true;
    }
    const inputDateTime = dayjs(`${day} ${value}`);
    const currentDateTime = dayjs.utc().locale("ja");
    const diffMinute = inputDateTime.diff(currentDateTime) / (1000 * 60);
    return diffMinute > 10;
  });
  // 終了日が開始日以降の場合true
  defineRule("to_date", (value: string, { fromDate }: Param) => {
    if (!fromDate) {
      return true;
    }
    const toDateTime = dayjs(value);
    const fromDateTime = dayjs(fromDate);
    return toDateTime.diff(fromDateTime) >= 0;
  });
  defineRule(
    "to_time",
    (value: string, { fromDate, fromTime, toDate }: Param) => {
      if (!fromDate || !fromTime || !toDate) {
        return true;
      }
      const fromDateTime = dayjs(`${fromDate} ${fromTime}`);
      const toDateTime = dayjs(`${toDate} ${value}`);
      return toDateTime.diff(fromDateTime) > 0;
    }
  );
};

const common = {
  searchText: "",
};

const names = {
  en: {
    ...common,
  },
};

const customMessage = {
  en: {
    ...en.messages,
  },
};

const messages = Object.assign({}, en.messages, customMessage.en);
const dictionary = Object.assign({}, en, { messages }, { names: names.en });

configure({
  generateMessage: localize("en", dictionary),
});
