import { createVuetify } from "vuetify";
import "@mdi/font/css/materialdesignicons.css";
import * as components from "vuetify/components";
import * as directives from "vuetify/directives";
import "vuetify/styles";

import type { UserModule } from "../types/models";

const vuetify = createVuetify({
  components,
  directives,
  icons: {
    defaultSet: "mdi",
  },
});

export const install: UserModule = ({ app }) => {
  app.use(vuetify);
};

export const vuetifyConfig = (state: any) => ({
  props: {
    "error-messages": state.errors,
    modelValue: state.value,
  },
});
