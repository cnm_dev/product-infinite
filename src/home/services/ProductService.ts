import BaseService, { METHODS } from "~/common/services/BaseService";
import { Product } from "../dtos/Product.dto";

type CommonResponse = {
  limit: number;
  products: Product[];
  skip: number;
  total: number;
};


class ProductService extends BaseService {
  constructor(prefix: string) {
    super(prefix);
  }

  async getProducts(limit: number, skip: number) : Promise<CommonResponse> {
    return await this.performRequest(
      METHODS.GET,
      `products?limit=${limit}&skip=${skip}`
    );
  }

  async searchProducts(name: string) : Promise<CommonResponse> {
    return await this.performRequest(
      METHODS.GET,
      `products/search?q=${name}`
    );
  }
}
export default new ProductService("");
