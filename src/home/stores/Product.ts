import { acceptHMRUpdate, defineStore } from "pinia";
import { Product } from "../dtos/Product.dto";
import ProductService from "../services/ProductService";

export const useProduct = defineStore(
  "products",
  () => {
    const products = ref<Product[]>([]);
    const nameProductSearch = ref<string>("");
    const productsSearch = ref<Product[]>([]);
    const totalProducts = ref<number>(0);

    async function getProducts(limit: number, skip: number) {
      const result = await ProductService.getProducts(limit, skip);

      if (result && result.products) {
        products.value = [...new Set([...products.value, ...result.products])];
        totalProducts.value = result.total;
      }
    }

    async function searchProducts() {
      const result = await ProductService.searchProducts(
        nameProductSearch.value
      );
      if (result && result.products) {
        productsSearch.value = [...result.products];
        // products.value = [...new Set([...products.value, ...result.products])]
      }
    }

    async function resetProducts() {
      products.value = [];
      nameProductSearch.value = "";
      productsSearch.value = [];
      totalProducts.value = 0;
    }

    return {
      products,
      productsSearch,
      totalProducts,
      nameProductSearch,
      getProducts,
      resetProducts,
      searchProducts,
    };
  },
  {
    persist: true,
  }
);
if (import.meta.hot)
  import.meta.hot.accept(acceptHMRUpdate(useProduct, import.meta.hot));
