// register vue composition api globally
import "vue-virtual-scroller/dist/vue-virtual-scroller.css";
import "./common/styles/style.scss";

import type { ViteSSGContext } from "vite-ssg";
import { setupLayouts } from "virtual:generated-layouts";
import generatedRoutes from "virtual:generated-pages";
import { ViteSSG } from "vite-ssg";
import App from "./App.vue";
const routes = setupLayouts(generatedRoutes);

export const createApp = ViteSSG(App, { routes }, (ctx) => {
  Object.values(
    import.meta.glob<{ install: (ctx: ViteSSGContext) => void }>(
      "./**/modules/*.ts",
      {
        eager: true,
      }
    )
  ).forEach((i) => i.install?.(ctx));
});
