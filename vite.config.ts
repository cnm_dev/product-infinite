/* eslint-disable camelcase */
import { defineConfig } from "vite";
import path from "path";
import vue from "@vitejs/plugin-vue";
import AutoImport from "unplugin-auto-import/vite";
import Components from "unplugin-vue-components/vite";
import Pages from "vite-plugin-pages";
import Layouts from "vite-plugin-vue-layouts";
import { VitePWA } from 'vite-plugin-pwa';


// https://vitejs.dev/config/
export default defineConfig({
  resolve: {
    alias: {
      "~/": `${path.resolve(__dirname, "src")}/`,
    },
  },
  plugins: [
    vue({
      include: [/\.vue$/],
      reactivityTransform: true,
    }),

    Pages({
      extensions: ["vue"],
      pagesDir: [{ dir: "src/**/pages", baseRoute: "" }],
    }),

    Layouts({
      layoutsDirs: "src/common/layouts",
    }),

    AutoImport({
      imports: [
        "vue",
        "vue-router",
        "vue-i18n",
        "vue/macros",
        "@vueuse/head",
        "@vueuse/core",
      ],
      dts: "src/auto-imports.d.ts",
      eslintrc: {
        enabled: true,
      },
    }),

    Components({
      dirs: ["src/**/components"],
      extensions: ["vue"],
      deep: true,
      dts: "src/components.d.ts",
      include: [/\.vue$/, /\.vue\?vue/, /\.md$/],
    }),

    VitePWA({
      registerType: "autoUpdate",
      includeAssets: ["**/*.{png,svg,jpg,ico}"],
    }),
  ],
});
